package helpers;

public enum Gender {
    MALE,
    FEMALE;

    private Gender() {
    }
}
