package helpers;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class DateFormatter {

    public static Date dateFromLocal(int year, int month, int day) {
        return Date.from(LocalDate.of(year, month, day).atStartOfDay(ZoneId.of("GMT")).toInstant());
    }
}
