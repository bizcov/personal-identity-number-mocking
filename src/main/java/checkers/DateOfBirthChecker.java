package checkers;

import helpers.DateFormatter;
import java.util.Date;

public class DateOfBirthChecker {

    int year, month, day;

    public Date getDateOfBirth(String egn) {
        month = Integer.parseInt(egn.substring(2,4));
        if(month > 12) {
            month = month - 40;
            year = Integer.parseInt("20" + egn.substring(0,2));
        } else {
            year = Integer.parseInt("19" + egn.substring(0,2));
        }
        day = Integer.parseInt(egn.substring(4,6));
        return DateFormatter.dateFromLocal(year, month, day);
    }
}
