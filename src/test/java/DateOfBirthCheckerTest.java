import checkers.DateOfBirthChecker;
import helpers.DataGenerator;
import helpers.DateFormatter;
import helpers.Gender;
import checkers.PersonalIdentityNumberGenerator;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Date;

public class DateOfBirthCheckerTest {

    DateOfBirthChecker dateOfBirthChecker = new DateOfBirthChecker();

    @Test
    public void checkDateOfBirthMock() {
        String egn = "5101044602";
        Date dateFromLocal = DateFormatter.dateFromLocal(1951, 1, 4);

        PersonalIdentityNumberGenerator mockito = mock(PersonalIdentityNumberGenerator.class);
        when(mockito.generateEgn(Gender.MALE.toString(), dateFromLocal)).thenReturn(egn);

        Date dateOfBirthCheck = dateOfBirthChecker.getDateOfBirth(egn);
        Assert.assertEquals(dateOfBirthCheck, dateFromLocal);
    }

    @Test
    public void checkDateOfBirthMockMillennium() {
        String egn = "0144073306";
        Date dateFromLocal = DateFormatter.dateFromLocal(2001, 4, 7);

        PersonalIdentityNumberGenerator mockito = mock(PersonalIdentityNumberGenerator.class);
        when(mockito.generateEgn(Gender.MALE.toString(), dateFromLocal)).thenReturn(egn);

        Date dateOfBirthCheck = dateOfBirthChecker.getDateOfBirth(egn);
        Assert.assertEquals(dateOfBirthCheck, dateFromLocal);
    }
}
