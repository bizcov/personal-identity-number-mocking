import checkers.DateOfBirthChecker;
import checkers.PersonalIdentityNumberGenerator;
import helpers.DataGenerator;
import helpers.DateFormatter;
import helpers.Gender;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.Date;

public class PersonalIdentityNumberGeneratorTest {

    PersonalIdentityNumberGenerator egnGenerator = new PersonalIdentityNumberGenerator();
    DataGenerator dataGenerator = new DataGenerator();
    int year, month, day;

    @BeforeTest
    public void generateDateValues() {
        year = dataGenerator.numberBetween(1900, 2020);
        month = dataGenerator.numberBetween(1, 12);
        day = dataGenerator.numberBetween(1, 28);
    }

    @Test
    public void checkEgnLength() {
        Date dateFromLocal = DateFormatter.dateFromLocal(year, month, day);
        String egn = egnGenerator.generateEgn(Gender.MALE.toString(), dateFromLocal);
        Assert.assertEquals(egn.length(), 10);
    }

    @Test
    public void checkDateOfBirth() {
        Date dateFromLocal = DateFormatter.dateFromLocal(year, month, day);
        String egn = egnGenerator.generateEgn(Gender.MALE.toString(), dateFromLocal);
        month = Integer.parseInt(egn.substring(2,4));
        if(month > 12) {
            month = month - 40;
            year = Integer.parseInt("20" + egn.substring(0,2));
        } else {
            year = Integer.parseInt("19" + egn.substring(0,2));
        }
        day = Integer.parseInt(egn.substring(4,6));
        Date dateOfBirthCheck = DateFormatter.dateFromLocal(year, month, day);

        Assert.assertEquals(dateOfBirthCheck, dateFromLocal);
    }

    @Test
    public void checkFemaleEgnFlag() {
        Date dateFromLocal = DateFormatter.dateFromLocal(year, month, day);
        String femaleEgn = egnGenerator.generateEgn(Gender.FEMALE.toString(), dateFromLocal);
        int femaleFlag = Integer.parseInt(femaleEgn.substring(8,9));
        Assert.assertEquals(femaleFlag % 2, 1);
    }
}
